"""基础分页器

1.默认10000页，可以认为是全部返还

2.get参数

"""

from rest_framework.pagination import LimitOffsetPagination


class BasePagination(LimitOffsetPagination):
    default_limit = 10000

    # 起始索引，页码×页数
    offset_query_param = "offset"

    # 每页数量
    limit_query_param = "limit"
