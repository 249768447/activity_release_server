"""抽象模型

1.定义了基本模型，包括创建时间、更新时间、软删的限制

"""

from django.db import models


class BaseModel(models.Model):
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    update_time = models.DateTimeField(verbose_name='更新时间', auto_now=True)
    is_delete = models.BooleanField(verbose_name='是否删除', default=False)

    def delete(self, *args, **kwargs):
        self.is_delete = True
        self.save()

    class Meta:
        abstract = True
