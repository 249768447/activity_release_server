"""参数校验

1.必填项

"""

from utils.response_wrapper.basic_response import DataPackage, jsonp_res_data, jsonp_wrapped_response


def verify_params(request_data: dict, require_data: list):
    """
    参数校验
    :param request_data:
    :param require_data:
    :return:
    """
    for k in require_data:
        if k not in request_data.keys() or request_data.get(k) is None:
            return jsonp_wrapped_response(jsonp_res_data(msg='{}是必填参数'.format(k), rc=1))
