"""用户认证及权限

1.json web token的生成和解密

2.基本的权限规则，主要分三类，admin/agent/user

Variables:
    JWT_SECRET {str} -- 加密解密json web token
"""
import time
import jwt
from django.conf import settings
from rest_framework.permissions import BasePermission
from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import NotAuthenticated
from user_manage.models import User

JWT_SECRET = settings.JWT_SECRET


# 使用json web token的方式生成token
def generate_token(user_dict: dict, expire: int = 3600 * 24 * 7):
    headers = {
        'alg': "HS256",
    }

    token_dict = {
        'iat': time.time(),
        'exp': expire + time.time()
    }

    for k, v in user_dict.items():
        token_dict[k] = v

    jwt_token = jwt.encode(token_dict, JWT_SECRET,
                           algorithm='HS256', headers=headers).decode('ascii')
    return jwt_token


# 解析jwt
def cert_token(jwt_token: str):
    try:
        user_info = jwt.decode(jwt_token, JWT_SECRET, algorithms=['HS256'])
        return user_info
    except Exception as e:
        print(e)
        return False


# 用户登录认证
class LoginAuthentication(BaseAuthentication):

    def authenticate(self, request):

        token = request.META.get('HTTP_AUTHORIZATION')
        if not token:
            return None, None
        userinfo = cert_token(token)
        if not userinfo:
            return 'anonymous', False
        user = User.objects.filter(is_delete=False, id=userinfo.get('id')).first()
        if user:
            return user, True

        return 'anonymous', False


# 用户登录授权
class IsAuthenticatedPermission(BasePermission):

    def has_permission(self, request, view):
        try:
            is_authenticated = request._auth
            if is_authenticated:
                return is_authenticated
            else:
                raise NotAuthenticated
        except AttributeError:
            raise NotAuthenticated


# 普通用户授权，可能是学生，下级等
class UserPermission(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        if isinstance(user, User):
            return True
        return False
