"""获取七牛上传token

1.缓存可以在views层做

Variables:
	QINIU_SECRET_KEY {str} -- [key]
	QINIU_ACCESS_KEY {str} -- [秘钥]
	QINIU_BUCKET_NAME {str} -- [存储空间]
	QINIU_TOKEN_EXPIRE {int} -- [时效]
"""

from qiniu import Auth
from django.conf import settings

QINIU_SECRET_KEY = settings.QINIU_SECRET_KEY
QINIU_ACCESS_KEY = settings.QINIU_ACCESS_KEY
QINIU_BUCKET_NAME = settings.QINIU_BUCKET_NAME
QINIU_TOKEN_EXPIRE = settings.QINIU_TOKEN_EXPIRE


# 生成七牛上传token
def generate_qiniu_token():

    qiniu_client = Auth(QINIU_ACCESS_KEY, QINIU_SECRET_KEY)
    token = qiniu_client.upload_token(
        bucket=QINIU_BUCKET_NAME, expires=QINIU_TOKEN_EXPIRE)
    return token
