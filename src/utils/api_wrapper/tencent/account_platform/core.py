"""公众号接口封装
1.获取access_token

2.获取openid

3.jssdk认证

4.获取userinfo
"""
from utils.api_wrapper.tencent.api.account_platform import access_token_api, jsapi_ticket_api, openid_api, userinfo_api
from utils.api_wrapper.tencent.base import APIBase


class TencentPlatFormApi(APIBase):

    # 获取access_token，官网失效7200，仅最新access_token是有效的
    def get_access_token(self):
        url = access_token_api.format(APPID=self.appid, APPSECRET=self.secret)
        req = self.send_req(url=url)

        return req['access_token']

    # 用于jssdk认证，默认时效7200
    def get_jsapi_ticket(self, access_token: str):
        url = jsapi_ticket_api.format(ACCESS_TOKEN=access_token)
        req = self.send_req(url=url)

        return req['ticket']

    # 获取openid，小程序和公众号方式不同
    def get_openid(self, code: str):
        url = openid_api.format(CODE=code, APPID=self.appid, SECRET=self.secret)
        req = self.send_req(url)

        return req['openid']

    # 解算用户信息
    def get_userinfo(self, access_token: str, openid: str):
        url = userinfo_api.format(ACCESS_TOKEN=access_token, APPID=self.appid, OPENID=openid)
        req = self.send_req(url)

        return req