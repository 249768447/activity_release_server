"""小程序接口封装
1.获取openid
"""
from utils.api_wrapper.tencent.api.mini_program import openid_api, access_token_api, userinfo_api
from utils.api_wrapper.tencent.api.pay import ufdoder_api
from utils.api_wrapper.tencent.base import APIBase
import hashlib
import random
import string
from django.conf import settings
import xmltodict
import requests
import time

class TencentMinProApi(APIBase):

    # access_tooken
    def get_access_token(self):
        url = access_token_api.format(APPID=self.appid, APPSECRET=self.secret)
        req = self.send_req(url=url)

        return req['access_token']

    # 请求openid
    def get_openid(self, code: str):
        params = {
            'appid':self.appid,
            'secret': self.secret,
            'js_code': code,
            'grant_type': 'authorization_code'
        }
        url = openid_api
        req = self.send_req(url, params=params)
        return req['openid']

    # 解算用户信息
    def get_userinfo(self, access_token: str, openid: str):
        url = userinfo_api.format(ACCESS_TOKEN=access_token, APPID=self.appid, OPENID=openid)
        req = self.send_req(url)

        return req

    # 签名算法，微信支付要用
    def generate_sign(self, param):
        stringA = ''
        ks = sorted(param.keys())
        # 参数排序
        for k in ks:
            stringA += (str(k) + '=' + str(param[k]) + '&')
        # 拼接商户KEY
        stringSignTemp = stringA + "key=" + settings.MCH_KEY
        # md5加密,也可以用其他方式
        hash_md5 = hashlib.md5(stringSignTemp.encode('utf8'))
        sign = hash_md5.hexdigest().upper()
        return sign

    def get_pay_url(self, orderid, openid, goodsPrice, **kwargs):
        """向微信支付端发出请求，获取url"""
        key = settings.MCH_KEY
        nonce_str = ''.join(random.sample(string.ascii_letters + string.digits, 30))  # 生成随机字符串，小于32位
        params = {
            'appid': self.appid,  # 小程序ID
            'mch_id': settings.MCH_ID,  # 商户号
            'nonce_str': nonce_str,  # 随机字符串
            "body": settings.PRODUCT_DESCRIP,  # 支付说明
            'out_trade_no': orderid,  # 生成的订单号
            'total_fee': str(int(goodsPrice) * 100),  # 标价金额
            'spbill_create_ip': "127.0.0.1",  # 小程序不能获取客户ip，web用socekt实现
            'notify_url': settings.WECHAT_ORDER_RESULT_NOTIFY,
            'trade_type': settings.TRAD_TYPE,  # 支付类型
            "openid": openid,  # 用户id
        }
        # 生成签名
        params['sign'] = self.generate_sign(params)

        # python3一种写法
        param = {'root': params}
        xml = xmltodict.unparse(param)
        response = requests.post(ufdoder_api, data=xml.encode('utf-8'), headers={'Content-Type': 'text/xml'})
        # xml 2 dict
        msg = response.content.decode('utf-8')
        print(msg)
        xmlmsg = xmltodict.parse(msg)
        # 4. 获取prepay_id
        if xmlmsg['xml']['return_code'] == 'SUCCESS':
            if xmlmsg['xml']['result_code'] == 'SUCCESS':
                prepay_id = xmlmsg['xml']['prepay_id']
                # 时间戳
                timeStamp = str(int(time.time()))
                # 5. 五个参数
                data = {
                    "appId": self.appid,
                    "nonceStr": nonce_str,
                    "package": "prepay_id=" + prepay_id,
                    "signType": 'MD5',
                    "timeStamp": timeStamp,
                }
                # 6. paySign签名
                paySign = self.generate_sign(data)
                data["paySign"] = paySign  # 加入签名
                # 7. 传给前端的签名后的参数
                return data
