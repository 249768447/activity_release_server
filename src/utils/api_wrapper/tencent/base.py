"""腾讯接口封装
1.请求retry

2.签名算法
"""

import hashlib
import requests
from django.conf import settings
from retry import retry
import json


class APIBase(object):

    appid = settings.WECHAT_APPID
    secret = settings.WECHAT_SECRET

    def __init__(self):
        pass

    # 请求失败，retry5次
    @retry(tries=5, delay=3)
    def send_req(self, url: str, params: dict = None,
                 data: dict = None, method: str = 'get'):
        try:
            req = requests.request(
                url=url,
                params=params,
                data=data,
                method=method
            )
            content = json.loads(req.content.decode('utf-8'))
            print(content)
            try:
                return content
            except Exception:
                return req.text
        except Exception as e:
            print(e)
            print('retry')

    # 签名算法
    def signutare_encryption(self, ret):

        string = '&'.join(['%s=%s' % (key.lower(), ret[key])
                           for key in sorted(ret)]).encode('utf-8')
        signature = hashlib.sha1(string).hexdigest()
        response = dict()
        response['nonceStr'] = ret.get('noncestr')
        response['signature'] = signature
        response['timestamp'] = ret.get('timestamp')

        return response
