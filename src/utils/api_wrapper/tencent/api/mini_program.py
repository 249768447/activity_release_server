# openid
openid_api = 'https://api.weixin.qq.com/sns/jscode2session'

# 获取access_token
access_token_api = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appId={APPID}&secret={APPSECRET}'

# 获取用户信息
userinfo_api = 'https://api.weixin.qq.com/sns/userinfo?access_token={ACCESS_TOKEN}&openid={OPENID}'