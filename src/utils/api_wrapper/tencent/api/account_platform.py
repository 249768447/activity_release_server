# 获取access_token
access_token_api = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appId={APPID}&secret={APPSECRET}'

# 获取jsapi_ticket
jsapi_ticket_api = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token={ACCESS_TOKEN}&type=jsapi'

# 获取openid
openid_api = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid={APPID}&secret={SECRET}&code={CODE}&grant_type=authorization_code'

# 获取用户信息
userinfo_api = 'https://api.weixin.qq.com/sns/userinfo?access_token={ACCESS_TOKEN}&openid={OPENID}'