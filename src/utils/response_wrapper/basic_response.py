"""请求封装

1.DataPackage数据封装，分数组和对象

2.BasicResponse响应封装，状态码、数据、信息


基础ajax请求返回协议：
    {
        'rc': 0,
        'msg': 'OK',
        'data': {},
    }

    rc(response code)存在的值：
    0        成功
    1        成功，但部分需求未满足
    4        常规错误
    5        内部服务异常
    7        外部接口错误
    9        请登录


"""

import json
import urllib
from django.http import HttpResponse, JsonResponse
from django.core.serializers.json import DjangoJSONEncoder


class DataPackage(object):

    def __init__(self, elements=[], fields=None):

        self._data = {'elements': []}
        if elements:
            self.set_elements(elements)

        if fields:
            self.set_fields(fields)

    def get_data(self):

        return self._data

    def set_elements(self, elements):

        if type(elements) == list or type(elements) == dict:
            self._data['elements'] = elements
        else:
            raise TypeError('elements must be a List or Dict')

        return self

    def set_fields(self, fields):

        self._data.update(fields)


class BasicResponse(object):

    def __init__(self, response_code=None, msg=None, data=None):
        self._res = {'rc': response_code, 'msg': msg, 'data': data}

    def set_rc(self, response_code):
        if type(response_code) == int:
            self._res['rc'] = response_code
        else:
            raise TypeError('response_code must be a Integer')

        return self

    def set_msg(self, msg):

        _msg = None
        if type(msg) == str:
            _msg = msg
        else:
            raise TypeError('msg must be a string')
        self._res['msg'] = _msg

        return self

    def set_data(self, data):

        if type(data) == DataPackage:
            self._res['data'] = data.get_data()
        else:
            raise TypeError('data s class must be DataPackage')

        return self

    def get_res(self):

        return self._res

    def serialize(self):
        if self._res['rc'] is None:
            raise AttributeError('response_code must be set')

        return json.dumps(self._res)


def jsonp_wrapped_response(data_res):

    if type(data_res) == BasicResponse:
        json_obj = data_res.get_res()
    else:
        raise TypeError('data_res s class must be BasicResponse')

    jsonresponse_obj = JsonResponse(json_obj, safe=False)
    return jsonresponse_obj


def jsonp_res_data(data=None, msg='', rc=0):

    response = BasicResponse()
    if data:
        response.set_data(data)
    response.set_msg(msg).set_rc(rc)

    return response
