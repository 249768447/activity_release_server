from channels.routing import ProtocolTypeRouter
from user_manage import routings as user_routings
from channels.sessions import SessionMiddlewareStack
from channels.routing import URLRouter

application = ProtocolTypeRouter({
    'websocket': SessionMiddlewareStack(
        URLRouter(user_routings.websocket_urlpatterns)
    )
})
