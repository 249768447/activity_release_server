import django
import os
from django.conf import settings
import jinja2
from django.urls.resolvers import URLResolver, URLPattern
import re


def recursion_urls(pre_namespace, pre_url, urlpatterns, resource):
    for item in urlpatterns:
        if isinstance(item, URLPattern):  # type1 URLPattern
            if not item.name:
                continue
            if pre_namespace:
                namespace = pre_namespace
            else:
                namespace = item.name
            url = pre_url + str(item.pattern)
            url = url.replace('<int', '').replace('<str', '').replace('>', '')
            link_params = re.findall(':(\w+)', url)

            link = {'name': item.name, 'url': url.replace('^', '').replace('$', ''), "meta": item.default_args}
            if link_params:
                link['meta']['link_params'] = link_params
            if not resource.get(namespace):
                resource[namespace] = [link]
            else:
                resource[namespace].append(link)
        elif isinstance(item, URLResolver):  # type2 URLResolver
            if item.namespace:
                namespace = item.namespace
            else:
                namespace = pre_namespace
            recursion_urls(namespace, pre_url + str(item.pattern), item.url_patterns, resource)


def get_all_url_dict(urlpatterns):
    resource = {}
    recursion_urls(None, '/', urlpatterns, resource)  # 递归去获取所有的路由
    return resource

    # urlpatterns = import_string("HouseAgent.urls.urlpatterns")


def render(urlpatterns, projectDir, namespaceTmpPath, storage_dirname):
    status = "create"
    resource = get_all_url_dict(urlpatterns)
    storage_dirname = storage_dirname or "apiMirror"
    apiDir = os.path.join(projectDir, storage_dirname)
    if not os.path.exists(apiDir):
        os.mkdir(apiDir)
    else:
        status = "update"
    for k in resource.keys():
        with open(namespaceTmpPath, "r+") as _nstp:
            raw = _nstp.read()
        content = jinja2.Template(raw).render(namespace=k, links=resource.get(k))
        with open(os.path.join(apiDir, k + '.js'), 'wb') as _tt:
            _tt.write(content.encode())
    print('success {status} apiMirror in <{apiDir}>'.format(apiDir=apiDir, status=status))
