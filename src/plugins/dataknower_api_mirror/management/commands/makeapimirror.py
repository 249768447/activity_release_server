from django.core.management import BaseCommand
from os import path
from os.path import dirname
from django.conf import settings
from django.utils.module_loading import import_string
from tools.dataknower_api_mirror.core import render



# from django.contrib.auth.management.commands.changepassword import


class Command(BaseCommand):
    def handle(self, *args, **options):
        storage_dirname = options.get("dirname")
        import sys
        appPath = dirname(dirname(dirname(path.abspath(__file__))))
        sys.path.append(appPath)
        namespaceTmpPath = path.join(appPath, "template", "tmp.tmp")
        projectPath = dirname(settings.BASE_DIR)
        projectName = settings.BASE_DIR.split("/")[-1]
        urlpatterns = import_string("{projectName}.urls.urlpatterns".format(projectName=projectName))
        render(urlpatterns, projectPath, namespaceTmpPath,storage_dirname)


    def add_arguments(self, parser):
        parser.add_argument(
            '-f', dest='dirname',
            help='Specifies the storage dirname to use. Default is "apiMirror".',
        )





if __name__ == '__main__':
    import sys
    appPath = dirname(dirname(dirname(path.abspath(__file__))))
    sys.path.append(appPath)
    namespaceTmpPath = path.join(appPath,"template","tmp.tmp")
    projectPath = dirname(settings.BASE_DIR)
    projectName = settings.BASE_DIR.split("/")[-1]
    urlpatterns = import_string("{projectName}.urls.urlpatterns".format(projectName=projectName))
    render(appPath,projectPath,namespaceTmpPath)


    # from dk-apiMirror.main import render
    #
    # render(BASE_DIR, namespaceTmpPath)


