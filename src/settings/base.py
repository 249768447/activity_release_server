import os
import sys

# src目为根目录
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
root = lambda *x: os.path.join(BASE_DIR, *x)

# 扩展python包查询目录
sys.path.insert(0, root('apps'))
sys.path.insert(0, root('plugins'))

# 密钥
SECRET_KEY = 'CHANGE THIS!!!'
JWT_SECRET = SECRET_KEY

# 是否调试模式
DEBUG = True

ALLOWED_HOSTS = ['*']

# 基础app
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'xadmin',
    'crispy_forms',
    'DjangoUeditor',
    'dataknower_api_mirror',
    'corsheaders',
    # 'channels'
]

# 项目app
PROJECT_APPS = [
    'user_manage',
    'activity_manage',
    'agent_manage',
    'setting_manage'
]

INSTALLED_APPS += PROJECT_APPS

# 中间件取消csrf
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# 根路由
ROOT_URLCONF = 'urls'

# wsgi
WSGI_APPLICATION = 'wsgi.application'

# 语言环境
LANGUAGE_CODE = 'zh-hans'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TPL_DIRS = [root('templates'), os.path.join(BASE_DIR, 'plugins', 'DjangoUeditor', 'static', 'ueditor')]
STATICFILES_DIRS = [root('static')]
if os.path.exists('/opt/activity_release_front_h5'):
    TPL_DIRS.append('/opt/activity_release_front_h5')
    STATICFILES_DIRS.append('/opt/activity_release_front_h5')

# 模板
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': TPL_DIRS,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    }
]

# 密码校验器
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# 日志
BASE_LOG_DIR = os.path.join(os.path.dirname(BASE_DIR), 'var', 'log', 'django')

if not os.path.exists(BASE_LOG_DIR):
    os.makedirs(BASE_LOG_DIR)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '[%(asctime)s][%(threadName)s:%(thread)d][task_id:%(name)s][%(filename)s:%(lineno)d]'
                      '[%(levelname)s][%(message)s]'
        },
        'simple': {
            'format': '[%(levelname)s][%(asctime)s][%(filename)s:%(lineno)d]%(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',  #
            'formatter': 'simple'
        },
        'default': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, "info.log"),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 3,
            'formatter': 'standard',
            'encoding': 'utf-8',
        },
        'error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, "err.log"),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 5,
            'formatter': 'standard',
            'encoding': 'utf-8',
        },
        'debug': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(BASE_LOG_DIR, "debug.log"),
            'maxBytes': 1024 * 1024 * 50,
            'backupCount': 5,
            'formatter': 'standard',
            'encoding': "utf-8"
        }
    },
    'loggers': {
        'django': {
            'handlers': ['default', 'console', 'error'],
            'level': 'INFO',
            'propagate': True,
        },
        'debug': {
            'handlers': ['console', 'debug'],
            'level': 'DEBUG',
        }
    },
}

PREFIX_URL = 'https://'

STATICFILES_DIRS = tuple(STATICFILES_DIRS)

if not os.path.exists(root('collectstatic')):
    os.mkdir(root('collectstatic'))

if not os.path.exists(root('static')):
    os.mkdir(root('static'))

USE_QINIU_FILE_STORAGE = False
USE_QINIU_STATIC_STORAGE = False

QINIU_ACCESS_KEY = 'Z2qvhrrcvWb2pMqcCJqfJ4KN_XA0KYuDuPwMdhNm'
QINIU_SECRET_KEY = '7lqqrfSmduOGi1mEcsZUjchXdXIVRKVkKXfzuc3M'
QINIU_BUCKET_NAME = 'fenjian'
QINIU_BUCKET_DOMAIN = 'qiniu.digfunny.com'
QINIU_SECURE_URL = False
QINIU_TOKEN_EXPIRE = 60 * 60 * 2

if USE_QINIU_STATIC_STORAGE:
    # 七牛存储
    STATIC_URL = PREFIX_URL + QINIU_BUCKET_DOMAIN + '/collectstatic/'
    STATIC_ROOT = 'collectstatic'
    STATICFILES_STORAGE = 'qiniustorage.backends.QiniuStaticStorage'
else:
    # 本地存储
    STATIC_URL = '/static/'
    STATIC_ROOT = root('collectstatic')

if USE_QINIU_FILE_STORAGE:
    # 七牛上传
    MEDIA_URL = PREFIX_URL + QINIU_BUCKET_DOMAIN + '/'
    MEDIA_ROOT = 'media'
    DEFAULT_FILE_STORAGE = 'qiniustorage.backends.QiniuMediaStorage'
else:
    # 本地上传
    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'utils.common.auth_utils.LoginAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'utils.common.auth_utils.IsAuthenticatedPermission',
    )
}

# channels配置
ASGI_APPLICATION = 'routings.application'
