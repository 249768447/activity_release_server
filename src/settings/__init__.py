from .base import *
import os

# 环境隔离
env = os.environ.get('ENV')
if env == 'pro':
    from .pro import *
elif env == 'dev':
    from .dev import *
