DEBUG = False

# 数据库
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'activity_release',
        'USER': 'activity_release',
        'PASSWORD': 'asd456...',
        'HOST': '47.102.144.67',
        'PORT': '3306',
    }
}

# 缓存
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://172.17.0.1:8766/0',
        'OPTIONS': {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "CONNECTION_POOL_KWARGS": {"max_connections": 100}
        }
    },
}

# jwt时效
TOKEN_EXPIRE = 60 * 60 * 24 * 365

# 跨域
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = ()
CORS_ALLOW_METHODS = (
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
    'VIEW',
)
CORS_ALLOW_HEADERS = (
    'accept',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
)

# appid和秘钥
WECHAT_APPID = 'wx9b2cd12cac88ce4d'
WECHAT_SECRET = '45bfce33f8145288f2cf12fadf2db114'
# 商户号
MCH_ID = 1559518821
MCH_KEY = '9511debdbfa0447c8c318546cbedd753'
# 微信支付成功后的回调地址
WECHAT_ORDER_RESULT_NOTIFY = 'https://activity.pychina.online/user/payback/'
# 商品描述
PRODUCT_DESCRIP = '羽毛球活动场地费+球费'
# 交易类型
TRAD_TYPE = 'JSAPI'

# chanales
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": ["redis://172.17.0.1:8766/1"],
        },
    },
}
