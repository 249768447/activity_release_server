from django.urls import path, re_path, include
from django.views.static import serve
from django.conf import settings
import xadmin
from DjangoUeditor.views import get_ueditor_controller
from django.views.generic.base import TemplateView

urlpatterns = [

    path('xadmin/', xadmin.site.urls),

    # 富文本配置
    re_path('ueditor/', include('DjangoUeditor.urls')),
    re_path('controller/', get_ueditor_controller),
    # 秀米编辑器
    path('public/ueditor/xiumi-ue-dialog-v5.html', TemplateView.as_view(template_name='xiumi-ue-dialog-v5.html')),
    re_path('^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),

    # apps
    path('user/', include('user_manage.urls')),
    path('activity/', include('activity_manage.urls')),
    path('agent/', include('agent_manage.urls')),
    path('setting/', include('setting_manage.urls')),

    # 公众号，废弃
    path('', TemplateView.as_view(template_name='index.html')),
    path('MP_verify_U5w6Z9v5KYPKa3S5.txt',
         TemplateView.as_view(template_name='MP_verify_U5w6Z9v5KYPKa3S5.txt', content_type='text/plain')),
]
