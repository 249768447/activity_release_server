from rest_framework import serializers
from . import models
import json


class ActivitySerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()
    def get_user(self, obj):
        userinfo = json.loads(obj.user.userinfo)
        userinfo['id'] = obj.user.id
        return userinfo

    def get_images(self, obj):
        images = obj.images.split('|')
        return images

    class Meta:
        model = models.Activity
        fields = '__all__'


class SignUpSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SignUp
        fields = '__all__'


class ScheduleSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Activity
        fields = '__all__'
