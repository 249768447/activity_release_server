from django.db import models
from utils.common.base_model import BaseModel
from user_manage import models as user_models
import json


class Activity(BaseModel):
    trading_type_choice = ((0, '线下付款'), (1, '预付款'), (2, '多退少补'))

    title = models.CharField(verbose_name='活动标题', max_length=255)
    min_limit = models.PositiveIntegerField(verbose_name='最少人数限制')
    max_limit = models.PositiveIntegerField(verbose_name='最多人数限制')
    cost = models.FloatField(verbose_name='费用')
    start_time = models.CharField(verbose_name='开始时间', max_length=255)
    end_time = models.CharField(verbose_name='结束时间', max_length=255, null=True, blank=True)
    brief = models.TextField(verbose_name='简介')
    images = models.TextField(verbose_name='图片， 用|分隔', null=True, blank=True)
    user = models.ForeignKey(verbose_name='创建者', to=user_models.User, on_delete=models.CASCADE, db_constraint=False)
    cancle_limit = models.PositiveIntegerField(verbose_name='活动开始前多久可以取消，单位小时', null=True, blank=True)
    latitude = models.CharField(verbose_name='纬度', max_length=50)
    longitude = models.CharField(verbose_name='经度', max_length=50)
    address = models.CharField(verbose_name='地址', max_length=255)
    cancle = models.BooleanField(verbose_name='是否取消', default=False)
    trading_type = models.PositiveIntegerField(verbose_name='付款方式', choices=trading_type_choice)

    def __str__(self):
        return '{} {}'.format(self.id, self.title)

    class Meta:
        verbose_name = '活动'
        verbose_name_plural = verbose_name
        db_table = 'activity'


class SignUp(BaseModel):
    activity = models.ForeignKey(verbose_name='活动', to=Activity, on_delete=models.CASCADE, db_constraint=False)
    user = models.ForeignKey(verbose_name='参与者', to=user_models.User, on_delete=models.CASCADE, db_constraint=False)
    cancle = models.BooleanField(verbose_name='是否取消', default=False)
    cost = models.FloatField(verbose_name='消费', null=True, blank=True)

    def __str__(self):
        if not self.user.userinfo:
            nickname = '匿名'
        else:
            nickname = json.loads(self.user.userinfo).get('nickName', '匿名')
        return '{} {} {}'.format(self.id, self.activity.title, nickname)

    class Meta:
        verbose_name = '报名'
        verbose_name_plural = verbose_name
        db_table = 'sigin_up'
