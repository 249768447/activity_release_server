import xadmin
from . import models
import json


class ActivityAdmin(object):
    list_display = ('id', 'title', 'min_limit', 'max_limit', 'cost', 'start_time', 'trading_type')


class SignUpAdmin(object):
    def username(self, obj):
        if not obj.user.userinfo:
            nickname = '匿名'
        else:
            nickname = json.loads(obj.user.userinfo).get('nickName', '匿名')
        return nickname

    def activity_title(self, obj):
        return obj.activity.title

    username.short_description = '用户'
    activity_title.short_description = '活动'

    list_display = ('id', 'activity_title', 'username')


xadmin.site.register(models.Activity, ActivityAdmin)
xadmin.site.register(models.SignUp, SignUpAdmin)
