from user_manage import models as user_models
from setting_manage import models as setting_models
from activity_manage import models as activity_models
from utils.response_wrapper.basic_response import DataPackage, jsonp_res_data, jsonp_wrapped_response
from utils.response_wrapper.basic_response_code import HALF_SUCCESS
from rest_framework import exceptions, permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet
from utils.common.verification import verify_params
from utils.common.auth_utils import UserPermission, IsAuthenticatedPermission
from utils.common.pagination import BasePagination
from django.core.serializers import serialize
from django.forms.models import model_to_dict
import datetime
from . import serializers
import json


class ScheduleViewSet(ModelViewSet):
    serializer_class = serializers.ScheduleSerializer
    queryset = activity_models.Activity.objects.filter(
        is_delete=False, cancle=False)
    permission_classes = (permissions.AllowAny,)

    # 某月排期
    def list(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id')
        year = request.GET.get('year')
        month = request.GET.get('month')
        if len(month) == 1:
            month = '0' + month
        query_month = '{}-{}'.format(year, month)
        activity_set = self.queryset.values('start_time', 'create_time').filter(user_id=user_id,
                                                                                start_time__contains=query_month).values(
            'start_time').distinct().order_by('start_time', 'create_time')

        ret = []
        for i in activity_set:
            ret.append(i['start_time'].split(' ')[0])
        datapackage = DataPackage(elements=ret)
        return jsonp_wrapped_response(
            jsonp_res_data(data=datapackage, msg='排期表'))

    # 某天活动详情
    def retrieve(self, request, *args, **kwargs):
        user_id = kwargs.get('user_id')
        query_date = request.GET.get('date').split('-')
        if len(query_date[1]) == 1:
            query_date[1] = '0' + query_date[1]
        if len(query_date[2]) == 1:
            query_date[2] = '0' + query_date[2]
        start_time = '-'.join(query_date)
        activity_set = self.queryset.filter(user_id=user_id, start_time__contains=start_time).order_by('start_time',
                                                                                                       'create_time')
        ret = []
        for i in activity_set:
            ser = serializers.ActivitySerializer(i)
            data = ser.data
            count = activity_models.SignUp.objects.filter(
                is_delete=False, cancle=False, activity_id=i.id).count()
            data['count'] = count
            _start_time = datetime.datetime.strptime(
                data['start_time'], '%Y-%m-%d %H:%M')
            now = datetime.datetime.now()
            if now < _start_time:

                seconds = (_start_time - now).total_seconds()
                # 折算天数
                days = int(seconds / (60 * 60 * 24))
                # 剩余秒
                _t = seconds - days * (60 * 60 * 24)
                # 折算小时
                hours = int(_t / (60 * 60))
                # 剩余秒
                _t = _t - hours * (60 * 60)
                # 折算分钟
                mins = int(_t / 60)
                # 剩余秒
                seconds = _t - (mins * 60)

                data['days'] = days
                data['hours'] = hours
                data['mins'] = mins
                data['secs'] = seconds
            else:
                data['days'] = 0
                data['hours'] = 0
                data['mins'] = 0
                data['secs'] = 0
            ret.append(data)
        datapackage = DataPackage(elements=ret)
        return jsonp_wrapped_response(jsonp_res_data(
            data=datapackage, msg='{}活动'.format(start_time)))


class ActivityViewSet(ModelViewSet):
    queryset = activity_models.Activity.objects.filter(is_delete=False)
    serializer_class = serializers.ActivitySerializer
    pagination_class = BasePagination

    def check_permissions(self, request):
        url = request.get_full_path()
        if 'detail' in url or 'list' in url:
            return True

        return super(ActivityViewSet, self).check_permissions(request)

    # 创建\更新
    def create(self, request, *args, **kwargs):
        json_data = json.loads(request.body)
        ver = verify_params(json_data,
                            ['address', 'brief', 'cancle', 'endTime', 'images', 'latitude', 'limit', 'longitude',
                             'price', 'startTime', 'title', 'tradeType'])
        if ver:
            return ver

        activity_id = int(kwargs.get('activity_id'))
        user = request.user
        if activity_id == 0:
            activity = activity_models.Activity(is_delete=False, user=user)
        else:
            activity = activity_models.Activity.objects.filter(
                is_delete=False, user=user)

        activity.address = json_data.get('address')
        activity.brief = json_data.get('brief')
        activity.title = json_data.get('title')
        activity.start_time = json_data.get('startTime')
        activity.end_time = json_data.get('endTime')
        activity.images = '|'.join(json_data.get('images'))
        activity.latitude = json_data.get('latitude')
        activity.longitude = json_data.get('longitude')
        activity.cost = float(json_data.get('price'))
        activity.cancle_limit = json_data.get('cancle')
        activity.min_limit = int(json_data.get('limit')[0])
        activity.max_limit = int(json_data.get('limit')[1])
        activity.trading_type = int(json_data.get('tradeType'))

        activity.save()

        return jsonp_wrapped_response(jsonp_res_data(msg='活动发布成功'))

    # 取消\恢复
    def update(self, request, *args, **kwargs):
        pass

    def retrieve(self, request, *args, **kwargs):
        activity_id = kwargs.get('activity_id')
        obj = self.queryset.filter(id=activity_id).first()
        ser = self.serializer_class(instance=obj, many=False)

        # 判断用户是否报名了
        user = request.user
        sigin_up = activity_models.SignUp.objects.filter(is_delete=False, cancle=False, activity_id=activity_id,
                                                         user=user).first()
        msg = 'yes' if sigin_up else 'no'

        datapackage = DataPackage(fields=ser.data)
        return jsonp_wrapped_response(jsonp_res_data(data=datapackage, msg=msg))

    # 列表，分角色
    def list(self, request, *args, **kwargs):

        query_user_id = request.GET.get('userId')
        if query_user_id:
            # 只保留今年的
            year = datetime.datetime.now().year
            self.queryset = self.queryset.filter(user_id=int(query_user_id), create_time__year=year)
        self.queryset = self.queryset.filter().order_by('-start_time', '-create_time')

        page = self.pagination_class()
        page_activity = page.paginate_queryset(
            queryset=self.queryset, request=request, view=self)

        ser = self.serializer_class(instance=page_activity, many=True)

        return page.get_paginated_response(ser.data)


class SignUpViewSet(ModelViewSet):
    queryset = activity_models.SignUp.objects.filter(is_delete=False)
    serializer_class = serializers.SignUpSerializer
    pagination_class = BasePagination

    # 报名
    def create(self, request, *args, **kwargs):
        user = request.user
        activity_id = kwargs.get('activity_id')
        activity = activity_models.Activity.objects.filter(is_delete=False, cancle=False, id=activity_id).first()

        # 校验人数
        required_num = activity.max_limit
        current = activity_models.SignUp.objects.filter(activity=activity, is_delete=False, cancle=False).count()
        if current >= required_num:
            print(required_num)
            print(current)
            return jsonp_wrapped_response(jsonp_res_data(msg='人数达到上限', rc=HALF_SUCCESS))
        # 校验付费（账户余额、定金）
        msg = '报名成功'
        if activity.trading_type == 0 and activity.cost != 0:
            # 预付款
            t = setting_models.OtherSetting.objects.filter(is_delete=False).first()
            all_cost = activity.cost + t.deposit if t else 0
            if all_cost > user.money:
                return jsonp_wrapped_response(jsonp_res_data(msg='余额不足', rc=HALF_SUCCESS))
            user.money -= all_cost
            if t:
                msg = '报名成功，定金{}'.format(t.deposit)

        elif activity.trading_type == 1 and activity.cost != 0:
            # 线下付款，定金
            t = setting_models.OtherSetting.objects.filter(is_delete=False).first()
            all_cost = t.deposit if t else 0
            if all_cost > user.money:
                return jsonp_wrapped_response(jsonp_res_data(msg='余额不足', rc=HALF_SUCCESS))
            user.money -= all_cost
            if t:
                msg = '报名成功，定金{}'.format(t.deposit)
        sigin_up, created = activity_models.SignUp.objects.update_or_create(
            user=user,
            activity=activity,
            defaults={
                'cancle': False
            }
        )
        ser = self.serializer_class(instance=sigin_up, many=False)
        datapackage = DataPackage(fields=ser.data)
        return jsonp_wrapped_response(jsonp_res_data(data=datapackage, msg=msg))

    # 取消报名
    def destroy(self, request, *args, **kwargs):
        # todo：判断下距离活动开始的时间，不在指定时间不允许取消
        user = request.user
        activity_id = kwargs.get('activity_id')
        activity = activity_models.Activity.objects.filter(is_delete=False, cancle=False, id=activity_id).first()
        cancle_limit = activity.cancle_limit
        if cancle_limit:
            t = datetime.datetime.now() + datetime.timedelta(hours=cancle_limit)
            if t > activity.start_time:
                msg = '活动开始前{}小时不允许取消'.format(t)
                return jsonp_wrapped_response(jsonp_res_data(msg=msg, rc=HALF_SUCCESS))

        sigin_up, created = activity_models.SignUp.objects.update_or_create(
            user=user,
            activity=activity,
            defaults={
                'cancle': True
            }
        )
        ser = self.serializer_class(instance=sigin_up, many=False)
        datapackage = DataPackage(fields=ser.data)
        return jsonp_wrapped_response(jsonp_res_data(data=datapackage, msg='取消成功'))

    # 报名列表
    def list(self, request, *args, **kwargs):
        activity_id = kwargs.get('activity_id')
        activity = activity_models.Activity.objects.filter(is_delete=False, id=activity_id).first()
        sigin_up_set = activity_models.SignUp.objects.filter(is_delete=False, cancle=False, activity=activity).order_by(
            'update_time')
        ret = []
        for i in sigin_up_set:
            # 头像、名称、是否付费
            userinfo = json.loads(i.user.userinfo)
            ret.append({
                'id': i.id,
                'avatarUrl': userinfo.get('avatarUrl'),
                'nickName': userinfo.get('nickName'),
                'cost': i.cost,
                'time': i.update_time.strftime('%H:%M:%S')
            })

        datapackage = DataPackage(elements=ret)
        return jsonp_wrapped_response(jsonp_res_data(data=datapackage, msg='报名信息'))


# 创建历史
class ActivityRecordViewSet(ModelViewSet):
    queryset = activity_models.Activity.objects.filter(is_delete=False)
    serializer_class = serializers.ActivitySerializer
    pagination_class = BasePagination

    def list(self, request, *args, **kwargs):
        user = request.user
        opt = kwargs.get('opt')
        if opt == 'created':
            self.queryset = self.queryset.filter(user=user).order_by('-start_time', '-update_time')

        elif opt == 'takePartIn':
            sigin_up = activity_models.SignUp.objects.values('activity_id').filter(user=user, is_delete=False,
                                                                                   cancle=False).order_by(
                '-update_time')
            sigin_up = list(sigin_up)
            activity_ids = [i['activity_id'] for i in sigin_up]
            self.queryset = self.queryset.filter(id__in=activity_ids).order_by('-start_time', '-update_time')

        page = self.pagination_class()
        page_activity = page.paginate_queryset(queryset=self.queryset, request=request, view=self)

        ser = self.serializer_class(instance=page_activity, many=True)

        return page.get_paginated_response(ser.data)


def created_list(request, *args, **kwargs):
    user = request.user
    queryset = activity_models.Activity.objects.filter(is_delete=False, user=user)

    pass
