from django.urls import path
from . import views

urlpatterns = [
    path('update/<int:activity_id>/', views.ActivityViewSet.as_view({'post': 'create'})),
    path('list/', views.ActivityViewSet.as_view({'get': 'list'})),
    path('detail/<int:activity_id>/', views.ActivityViewSet.as_view({'get': 'retrieve'})),
    path('delete/<int:activity_id>/', views.ActivityViewSet.as_view({'delete': 'destroy'})),
    path('status/<str:status>/<int:activity_id>/', views.ActivityViewSet.as_view({'update': 'update'})),
    path('schedule/list/<int:user_id>/', views.ScheduleViewSet.as_view({'get': 'list'})),
    path('schedule/detail/<int:user_id>/', views.ScheduleViewSet.as_view({'get': 'retrieve'})),
    path('siginUp/create/<int:activity_id>/', views.SignUpViewSet.as_view({'post': 'create'})),
    path('siginUp/delete/<int:activity_id>/', views.SignUpViewSet.as_view({'delete': 'destroy'})),
    path('siginUp/list/<int:activity_id>/', views.SignUpViewSet.as_view({'get': 'list'})),
    path('record/<str:opt>/list/', views.ActivityRecordViewSet.as_view({'get': 'list'})),

]
