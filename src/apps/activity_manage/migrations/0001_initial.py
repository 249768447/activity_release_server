# Generated by Django 2.2.7 on 2020-01-26 14:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('user_manage', '0002_auto_20200126_0200'),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name='更新时间')),
                ('is_delete', models.BooleanField(default=False, verbose_name='是否删除')),
                ('title', models.CharField(max_length=255, verbose_name='活动标题')),
                ('min_limit', models.PositiveIntegerField(verbose_name='最少人数限制')),
                ('max_limit', models.PositiveIntegerField(verbose_name='最多人数限制')),
                ('cost', models.FloatField(verbose_name='费用')),
                ('start_time', models.CharField(max_length=255, verbose_name='开始时间')),
                ('end_time', models.CharField(blank=True, max_length=255, null=True, verbose_name='结束时间')),
                ('brief', models.TextField(verbose_name='简介')),
                ('images', models.TextField(blank=True, null=True, verbose_name='图片， 用|分隔')),
                ('cancle_limit', models.PositiveIntegerField(blank=True, null=True, verbose_name='活动开始前多久可以取消')),
                ('latitude', models.CharField(max_length=50, verbose_name='纬度')),
                ('longitude', models.CharField(max_length=50, verbose_name='经度')),
                ('address', models.CharField(max_length=255, verbose_name='地址')),
                ('cancle', models.BooleanField(default=False, verbose_name='是否取消')),
                ('user', models.ForeignKey(db_constraint=False, on_delete=django.db.models.deletion.CASCADE, to='user_manage.Agent', verbose_name='创建者')),
            ],
            options={
                'verbose_name': '活动',
                'verbose_name_plural': '活动',
                'db_table': 'activity',
            },
        ),
        migrations.CreateModel(
            name='SignUp',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name='更新时间')),
                ('is_delete', models.BooleanField(default=False, verbose_name='是否删除')),
                ('cancle', models.BooleanField(default=False, verbose_name='是否取消')),
                ('cost', models.FloatField(blank=True, null=True, verbose_name='消费')),
                ('activity', models.ForeignKey(db_constraint=False, on_delete=django.db.models.deletion.CASCADE, to='activity_manage.Activity', verbose_name='活动')),
                ('user', models.ForeignKey(db_constraint=False, on_delete=django.db.models.deletion.CASCADE, to='user_manage.User', verbose_name='参与者')),
            ],
            options={
                'verbose_name': '报名',
                'verbose_name_plural': '报名',
                'db_table': 'sigin_up',
            },
        ),
    ]
