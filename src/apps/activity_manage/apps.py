from django.apps import AppConfig


class ActivityManageConfig(AppConfig):
    name = 'activity_manage'
    verbose_name = '活动管理'
