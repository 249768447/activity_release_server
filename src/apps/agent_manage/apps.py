from django.apps import AppConfig


class AgentManageConfig(AppConfig):
    name = 'agent_manage'
