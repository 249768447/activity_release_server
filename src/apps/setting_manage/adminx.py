import xadmin
from . import models
from xadmin import views


# 配置后台主题
class BaseSetting(object):
    enable_themes = True
    use_bootswatch = True


class GlobalSetting(object):
    site_title = '活动行后台管理系统'
    site_footer = '逍遥哥哥提供技术支出'
    menu_style = 'accordion'


class HelpTextAdmin(object):
    style_fields = {
        'content': 'ueditor'
    }


class ServerTextAdmin(object):
    style_fields = {
        'content': 'ueditor'
    }


class AboudTextAdmin(object):
    style_fields = {
        'content': 'ueditor'
    }


class OtherAdmin(object):
    list_display = ('id', 'deposit',)


xadmin.site.register(models.HelpText, HelpTextAdmin)
xadmin.site.register(models.ServerText, ServerTextAdmin)
xadmin.site.register(models.OtherSetting, OtherAdmin)
xadmin.site.register(models.AboudText, AboudTextAdmin)
xadmin.site.register(views.BaseAdminView, BaseSetting)
xadmin.site.register(views.CommAdminView, GlobalSetting)
