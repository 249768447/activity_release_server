from django.urls import path
from . import views

urlpatterns = [
    path('help/', views.help_view),
    path('about/', views.about_view),
    path('server/', views.server_view)
]
