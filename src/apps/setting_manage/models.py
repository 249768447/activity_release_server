from django.db import models
from utils.common.base_model import BaseModel
from DjangoUeditor.models import UEditorField


class HelpText(BaseModel):
    content = UEditorField(verbose_name='内容', default='', imagePath='images', filePath='files')

    def save(self, *args, **kwargs):
        HelpText.objects.all().delete()
        super(HelpText, self).save(args, kwargs)

    def __str__(self):
        enable = '不可用' if self.is_delete else '可用'
        return '{} {}'.format(self.id, enable)

    class Meta:
        verbose_name = '帮助中心'
        verbose_name_plural = verbose_name
        db_table = 'help_text'


class ServerText(BaseModel):
    content = UEditorField(verbose_name='内容', default='', imagePath='images', filePath='files', toolbars="full")

    def save(self, *args, **kwargs):
        ServerText.objects.all().delete()
        super(ServerText, self).save(args, kwargs)

    def __str__(self):
        enable = '不可用' if self.is_delete else '可用'
        return '{} {}'.format(self.id, enable)

    class Meta:
        verbose_name = '服务条款'
        verbose_name_plural = verbose_name
        db_table = 'server_text'


class AboudText(BaseModel):
    content = UEditorField(verbose_name='内容', default='', imagePath='images', filePath='files')

    def save(self, *args, **kwargs):
        AboudText.objects.all().delete()
        super(AboudText, self).save(args, kwargs)

    def __str__(self):
        enable = '不可用' if self.is_delete else '可用'
        return '{} {}'.format(self.id, enable)

    class Meta:
        verbose_name = '关于我们'
        verbose_name_plural = verbose_name
        db_table = 'about_text'


class OtherSetting(BaseModel):
    deposit = models.FloatField(verbose_name='定金', default=1)

    def save(self, *args, **kwargs):
        OtherSetting.objects.all().delete()
        super(OtherSetting, self).save(args, kwargs)

    def __str__(self):
        enable = '不可用' if self.is_delete else '可用'
        return '{} {}'.format(self.id, enable)

    class Meta:
        verbose_name = '其他设置'
        verbose_name_plural = verbose_name
        db_table = 'other_setting'
