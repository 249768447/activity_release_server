# Generated by Django 3.0.3 on 2020-03-06 21:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('setting_manage', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='OtherSetting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name='更新时间')),
                ('is_delete', models.BooleanField(default=False, verbose_name='是否删除')),
                ('deposit', models.FloatField(default=1, verbose_name='定金')),
            ],
            options={
                'verbose_name': '其他设置',
                'verbose_name_plural': '其他设置',
                'db_table': 'other_setting',
            },
        ),
    ]
