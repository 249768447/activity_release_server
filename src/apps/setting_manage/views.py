from django.http import HttpResponse
from setting_manage import models as setting_models


def help_view(request, *args, **kwargs):
    help_obj = setting_models.HelpText.objects.filter(is_delete=False).order_by('-update_time').first()
    help_content = help_obj.content if help_obj else ''

    return HttpResponse(content=help_content)


def about_view(request, *args, **kwargs):
    about_obj = setting_models.AboudText.objects.filter(is_delete=False).order_by('-update_time').first()
    about_content = about_obj.content if about_obj else ''

    return HttpResponse(content=about_content)


def server_view(request, *args, **kwargs):
    server_obj = setting_models.ServerText.objects.filter(is_delete=False).order_by('-update_time').first()
    server_content = server_obj.content if server_obj else ''

    return HttpResponse(content=server_content)
