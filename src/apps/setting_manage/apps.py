from django.apps import AppConfig


class SettingManageConfig(AppConfig):
    name = 'setting_manage'
    verbose_name = '配置中心'
