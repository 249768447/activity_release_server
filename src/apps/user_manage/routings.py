from . import consumers
from django.urls import path

websocket_urlpatterns = [
    path('ws/msg/<str:room_name>/', consumers.AsyncConsumer),
]
