from django.urls import path
from . import views

urlpatterns = [
    path('api/qiniu/token/', views.qiniu_token),
    path('login/', views.UserViewSet.as_view({'post': 'create'})),
    path('detail/', views.UserViewSet.as_view({'get': 'retrieve'})),
    path('update/', views.UserViewSet.as_view({'post': 'update'})),
    path('focusOn/<str:status>/<int:userId>/', views.FocusViewSet.as_view({'get': 'retrieve'})),
    path('focusOn/list/', views.FocusViewSet.as_view({'get': 'list'})),
    path('recharge/', views.TradingRecordViewSet.as_view({'post': 'create'})),
    path('payback/', views.payback),
    path('setting/qrcode/', views.setting_qr_code),
    path('money/', views.UserViewSet.as_view({'get': 'money'}))
]
