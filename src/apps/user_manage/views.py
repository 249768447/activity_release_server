from user_manage import models as user_models
from user_manage import serializers
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import permission_classes, api_view
from rest_framework import permissions
from rest_framework import exceptions
from utils.response_wrapper.basic_response import DataPackage, jsonp_res_data, jsonp_wrapped_response
from django.views.decorators.cache import cache_page
from utils.common.pagination import BasePagination
from utils.common.auth_utils import UserPermission
from django.conf import settings
from utils.common.verification import verify_params
from utils.api_wrapper.tencent.mini_program import TencentMinProApi
from utils.api_wrapper.qiniu.core import generate_qiniu_token
from utils.common.auth_utils import generate_token
from utils.response_wrapper.basic_response_code import API_ERROR
from django.http import HttpResponse
import time
import random
import string
import json
import logging
import xmltodict

logger = logging.getLogger('debug')


@api_view(['GET'])
@cache_page(timeout=60 * 5)
def qiniu_token(request, *args, **kwargs):
    token = generate_qiniu_token()
    logger.debug('更新七牛token')

    datapackage = DataPackage(fields={
        'token': token,
        'domain': settings.QINIU_BUCKET_DOMAIN
    })

    return jsonp_wrapped_response(jsonp_res_data(data=datapackage, msg='获取七牛token'))


class UserViewSet(ModelViewSet):
    serializer_class = serializers.UserSerializer
    pagination_class = BasePagination
    queryset = user_models.User.objects.filter(is_delete=False)
    permission_classes = (UserPermission,)

    def check_permissions(self, request):
        if '/user/login/' in request.get_full_path():
            return True
        return super(UserViewSet, self).check_permissions(request)

    # 登录
    def create(self, request, *args, **kwargs):
        json_data = json.loads(request.body)
        ver = verify_params(json_data, ['code'])
        if ver:
            return ver
        code = json_data.get('code')
        api = TencentMinProApi()

        openid = api.get_openid(code)

        user, created = user_models.User.objects.get_or_create(
            openid=openid
        )

        ser = self.serializer_class(instance=user)
        token = generate_token(ser.data, settings.TOKEN_EXPIRE)
        datapackage = DataPackage(fields={
            'token': token,
            'userId': user.id,
            'qr_code': user.qr_code
        })

        return jsonp_wrapped_response(jsonp_res_data(data=datapackage, msg='用户登录'))

    # 判断是否有用户详情
    def retrieve(self, request, *args, **kwargs):
        user_id = int(request.GET.get('userId'))
        user = user_models.User.objects.filter(is_delete=False, id=user_id).first()
        userinfo = user.userinfo
        userinfo = json.loads(userinfo)
        userinfo['id'] = user.id

        # 是否关注
        focus = user_models.FocusOn.objects.filter(is_delete=False, from_user_id=request.user.id,
                                                   to_user_id=user.id).first()
        userinfo['focus'] = bool(focus)

        has_userinfo = 'yes' if userinfo else 'no'
        datapackage = DataPackage(fields=userinfo)
        return jsonp_wrapped_response(jsonp_res_data(data=datapackage, msg=has_userinfo))

    # 更新用户信息
    def update(self, request, *args, **kwargs):
        user = request.user
        json_data = json.loads(request.body.decode())
        userinfo = json_data.get('userinfo')
        user.userinfo = json.dumps(userinfo)
        user.save()
        return jsonp_wrapped_response(jsonp_res_data(msg='更新用户信息成功'))

    # 获取财富
    def money(self, request, *args, **kwargs):
        user = request.user
        datapackage = DataPackage(fields={'money': round(user.money, 2)})
        return jsonp_wrapped_response(jsonp_res_data(data=datapackage, msg='财富值'))


class FocusViewSet(ModelViewSet):
    queryset = user_models.FocusOn.objects.filter(is_delete=False)
    serializer_class = serializers.FocusSerializer
    pagination_class = BasePagination

    # 关注\取消
    def retrieve(self, request, *args, **kwargs):
        user = request.user
        user_id = kwargs.get('userId')
        status = kwargs.get('status')
        status = False if status == 'yes' else True
        user_models.FocusOn.objects.update_or_create(
            from_user=user,
            to_user_id=user_id,
            defaults={
                'is_delete': status
            }
        )

        return jsonp_wrapped_response(jsonp_res_data(msg='操作成功'))

    # 关注列表
    def list(self, request, *args, **kwargs):
        user = request.user
        self.queryset = self.queryset.filter(from_user=user).order_by('-update_time')
        page = self.pagination_class()
        page_focuson = page.paginate_queryset(
            queryset=self.queryset, request=request, view=self)

        ser = self.serializer_class(instance=page_focuson, many=True)

        return page.get_paginated_response(ser.data)


class TradingRecordViewSet(ModelViewSet):
    queryset = user_models.TradingRecord.objects.filter(is_delete=False)
    serializer_class = serializers.TradingRecordSerializer
    pagination_class = BasePagination

    # 充值
    def create(self, request, *args, **kwargs):
        user = request.user
        price = json.loads(request.body.decode()).get('price')
        order_id = ''.join([random.choice(string.ascii_lowercase) for i in range(6)]) + str(
            int(time.time() * 1000000)) + ''.join([random.choice(string.ascii_lowercase) for i in range(6)])
        api = TencentMinProApi()
        data = api.get_pay_url(orderid=order_id, openid=user.openid, goodsPrice=price)
        if not data:
            return jsonp_wrapped_response(jsonp_res_data(msg='充值失败', rc=API_ERROR))
        datapackage = DataPackage(fields=data)
        return jsonp_wrapped_response(jsonp_res_data(data=datapackage, msg='充值成功'))

        # 充值最后一步由前端完成，完成后，前端要求查订单，订单存在才算真的成功

    # 返款
    def destroy(self, request, *args, **kwargs):
        pass

    # 交易记录
    def list(self, request, *args, **kwargs):
        pass


# 支付成功后的回调
def payback(request):
    msg = request.body.decode('utf-8')
    xmlmsg = xmltodict.parse(msg)
    print(xmlmsg)
    return_code = xmlmsg['xml']['return_code']

    if return_code == 'FAIL':
        # 官方发出错误
        return HttpResponse("""<xml><return_code><![CDATA[FAIL]]></return_code>
                            <return_msg><![CDATA[Signature_Error]]></return_msg></xml>""",
                            content_type='text/xml', status=200)
    elif return_code == 'SUCCESS':
        # 拿到这次支付的订单号
        out_trade_no = xmlmsg['xml']['out_trade_no']
        openid = xmlmsg['xml']['openid']
        total_fee = xmlmsg['xml']['total_fee']
        user = user_models.User.objects.get(is_delete=False, openid=openid)
        user_models.TradingRecord.objects.create(
            user=user,
            cost=float(total_fee),
            orderid_system=out_trade_no,
            orderid_tencent=out_trade_no,
            trading_type=0,
            tencent_info=msg
        )
        user.money = user.money + float(total_fee) / 100
        user.save()
        return HttpResponse("""<xml><return_code><![CDATA[SUCCESS]]></return_code>
                            <return_msg><![CDATA[OK]]></return_msg></xml>""",
                            content_type='text/xml', status=200)


# 设置用户的二维码
@api_view(['POST'])
def setting_qr_code(request, *args, **kwargs):
    json_data = json.loads(request.body.decode())
    qr_code_url = json_data.get('qrCodeUrl')
    user = request.user
    user.qr_code = qr_code_url
    user.save()
    datapackage = DataPackage(fields={'qr_code': qr_code_url})
    return jsonp_wrapped_response(jsonp_res_data(data=datapackage, msg='更新二维码成功'))
