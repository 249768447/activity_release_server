import xadmin
from . import models
import json


class UserAdmin(object):

    def qr_img(self, obj):
        div = '<img src="{}"  width="100" height="100"  alt = "二维码"'.format(obj.qr_code)
        return div

    qr_img.short_description = '二维码'
    qr_img.allow_tags = True

    def nickname(self, obj):
        if not obj.userinfo:
            nickname = '匿名'
        else:
            nickname = json.loads(obj.userinfo).get('nickName', '匿名')
        return nickname

    nickname.short_description = '用户名'

    list_display = ('id', 'nickname', 'money', 'qr_img')
    readonly_fields = ('userinfo', 'money')
    search_fields = ('userinfo',)


class TradingRecordAdmin(object):
    def nickname(self, obj):
        if not obj.user.userinfo:
            nickname = '匿名'
        else:
            nickname = json.loads(obj.user.userinfo).get('nickName', '匿名')
        return nickname

    list_display = ('id', 'trading_type', 'orderid_tencent', 'cost', 'nickname')


xadmin.site.register(models.User, UserAdmin)
xadmin.site.register(models.TradingRecord, TradingRecordAdmin)
