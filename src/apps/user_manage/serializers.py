from rest_framework import serializers
from user_manage import models
import json


class UserSerializer(serializers.ModelSerializer):
    userinfo = serializers.SerializerMethodField()

    def get_userinfo(self, obj):
        if not obj.userinfo:
            return '匿名'
        else:
            return json.loads(obj.userinfo).get('nickName')

    class Meta:
        model = models.User
        fields = '__all__'


class FocusSerializer(serializers.ModelSerializer):
    to_user = serializers.SerializerMethodField()

    def get_to_user(self, obj):
        userinfo = json.loads(obj.to_user.userinfo)
        userinfo['id'] = obj.to_user.id
        return userinfo

    class Meta:
        model = models.FocusOn
        fields = '__all__'


class TradingRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TradingRecord
        fields = '__all__'
