from django.apps import AppConfig


class UserManageConfig(AppConfig):
    name = 'user_manage'
    verbose_name = '用户管理'
