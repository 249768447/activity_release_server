# Generated by Django 2.2.7 on 2020-01-29 13:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user_manage', '0003_auto_20200129_1246'),
    ]

    operations = [
        migrations.CreateModel(
            name='FocusOn',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_time', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('update_time', models.DateTimeField(auto_now=True, verbose_name='更新时间')),
                ('is_delete', models.BooleanField(default=False, verbose_name='是否删除')),
                ('from_user', models.ForeignKey(db_constraint=False, on_delete=django.db.models.deletion.CASCADE, related_name='from_user', to='user_manage.User', verbose_name='关注者')),
                ('to_user', models.ForeignKey(db_constraint=False, on_delete=django.db.models.deletion.CASCADE, related_name='to_user', to='user_manage.User', verbose_name='被关注者')),
            ],
            options={
                'verbose_name': '关注',
                'verbose_name_plural': '关注',
                'db_table': 'focus_on',
            },
        ),
    ]
