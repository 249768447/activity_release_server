from django.db import models
from utils.common.base_model import BaseModel
import json
# from activity_manage import models as activity_models


class UserRank(BaseModel):
    name = models.CharField(verbose_name='用户等级', max_length=255)
    index = models.PositiveIntegerField(verbose_name='排序', unique=True)

    class Meta:
        verbose_name = '代理等级'
        verbose_name_plural = verbose_name
        db_table = 'user_rank'

    def __str__(self):
        return '<{}> <{}> <{}>'.format(self.id, self.name, self.index)


class User(BaseModel):
    openid = models.CharField(verbose_name='公众号openid', max_length=255, editable=False)
    userinfo = models.TextField(verbose_name='用户信息')
    rank = models.ForeignKey(verbose_name='等级', to=UserRank, on_delete=models.SET_NULL, null=True, blank=True)
    score = models.PositiveIntegerField(verbose_name='积分', default=0)
    money = models.FloatField(verbose_name='余额', default=0)
    qr_code = models.URLField(verbose_name='二维码', null=True, blank=True)

    class Meta:
        verbose_name = '用户'
        verbose_name_plural = verbose_name
        db_table = 'user'

    def __str__(self):
        if not self.userinfo:
            nickname = '匿名'
        else:
            nickname = json.loads(self.userinfo).get('nickName', '匿名')
        return '{} {}'.format(self.id, nickname)


class FocusOn(BaseModel):
    from_user = models.ForeignKey(verbose_name='关注者', to=User, on_delete=models.CASCADE, db_constraint=False,
                                  related_name='from_user')
    to_user = models.ForeignKey(verbose_name='被关注者', to=User, on_delete=models.CASCADE, db_constraint=False,
                                related_name='to_user')

    class Meta:
        verbose_name = '关注'
        verbose_name_plural = verbose_name
        db_table = 'focus_on'


class TradingRecord(BaseModel):
    trading_type_choice = ((0, '支出'), (1, '收益'))
    user = models.ForeignKey(verbose_name='用户', to=User, on_delete=models.DO_NOTHING, null=True, blank=True,
                             db_constraint=False)
    cost = models.FloatField(verbose_name='金额', default=0)
    orderid_system = models.CharField(verbose_name='系统订单号', max_length=255, unique=True, null=True, blank=True)
    orderid_tencent = models.CharField(verbose_name='腾讯订单号', max_length=255, unique=True, null=True, blank=True)
    trading_type = models.PositiveIntegerField(verbose_name='交易类型', choices=trading_type_choice)
    tencent_info = models.CharField(verbose_name='腾讯反馈信息', max_length=2550, null=True, blank=True)

    class Meta:
        verbose_name = '交易记录'
        verbose_name_plural = verbose_name
        db_table = 'trading_record'
